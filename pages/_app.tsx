import { FC } from 'react';
import type { AppProps } from 'next/app';

import '../styles/globals.scss';

const MyApp: FC<AppProps> = ({ Component, pageProps }) => {
  return <Component {...pageProps} />;
}

export default MyApp;
