import { FC, ReactNode } from 'react';
import Head from 'next/head';

import styles from './Layout.module.scss';

type Props = {
  children: ReactNode;
  title: string;
  description: string;
};

const Layout: FC<Props> = ({ children, title, description }) => {
  return (
    <>
      <Head>
        <title>{title}</title>
        <meta name="description" content={description} />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.app}>
        {children}
      </main>
    </>
  );
};

export { Layout };
